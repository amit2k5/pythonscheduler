import JobScheduler
import datetime
from crontab import CronTab

#!<File path to be determined
#
# Description: Python Client code to s=invoke the scheduler functionalities
#
# Prerequisites: <To be updated later>
#
# Author:       Amit Kumar - Wipro
#
# History:      Initial Version         October 2018

filePath = "/etc/opt/migration/wipro/wipro-sched.txt"
print("Now invoking the class")
jobs = JobScheduler.JobScheduler()
jobs.testMethod()
jobList = jobs.readCommands(filePath)

#cron instance
my_cron = CronTab(user='root')

for x in jobList:
	print("The command is: ",x)
	cronJobList = jobs.formCommand(x)
	print(cronJobList)
	#To be deleted
	print("Command: ",cronJobList[4])
	print("Minute: ", cronJobList[0])
	print("Hour: ", cronJobList[1])
	print("Day: ", cronJobList[2])
	print("Month: ", cronJobList[3])
	cjob = my_cron.new(command=cronJobList[4])
	cjob.minute.every(cronJobList[0])
	cjob.hour.every(cronJobList[1])
	cjob.dow.every(cronJobList[2])
	cjob.month.on(cronJobList[3])
	
	my_cron.write()
