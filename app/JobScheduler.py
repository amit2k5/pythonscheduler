import datetime
#!<File path to be determined
#
# Description: Python Script to schedule Command Execution
#
# Prerequisites: <To be updated later>
#
# Author:       Amit Kumar - Wipro
#
# History:      Initial Version         October 2018

class JobScheduler:

    #Test module to see if the invocation is correct
    def testMethod(self):
        return "Hello Method"
    
    #module to read the commands from a file and return the list
    def readCommands(self, filePath):
        print("Invoking the ReadCommands method")
        f = open(filePath, "r") #opening the file in ReadOnly mode
        myCommands = []
        for line in f:
            myCommands.append(line)
        f.close()
        return myCommands

    #module to fetch the string and bifurcate it into date, time and command
    def formCommand(self, commandString):
        str = commandString.split(';')

        #Fetch Minute and Hour
        minuteHourList = self.getMinuteHour(str[0])
        print("To be deleted - mmhh: ",minuteHourList[0], minuteHourList[1])

        #Form Date 
        cmdDate = str[1]+str[2]+'20'+str[3]
        format_str = '%d%m%Y' #The format
        datetime_obj = datetime.datetime.strptime(cmdDate, format_str)
        print("To be deleted - Date: ",datetime_obj)

        #Fetch Day and Month
        dayVal = self.getValOfDay(datetime_obj)
        print("To be deleted - DayVal: ",dayVal)
        monthVal = self.getValOfMonth(datetime_obj)
        print("To be deleted - MonthVal: ",monthVal)
        
        #Get all the values
        commandStruct = []
        commandStruct.append(int(minuteHourList[0])) #minute
        commandStruct.append(int(minuteHourList[1])) #hour
        commandStruct.append(int(dayVal)) #day of month
        commandStruct.append(int(monthVal)) #month
        commandStruct.append(str[4]) #command
        return commandStruct

    def getMinuteHour(self, mmhh):
        mmhhVal = mmhh.split(':')
        mmhhList = []
        mmhhList.append(mmhhVal[0])
        mmhhList.append(mmhhVal[1])
        return mmhhList

    def getValOfDay(self, srcDate):
        #Numberic Day: datetime_obj.day
	#return srcDate.strftime('%A')	
	return srcDate.day        
	

    def getValOfMonth(self, sourceDate):
        return sourceDate.month
                        

        

        

        
