from distutils.core import setup

setup(
    # Application name:
    name="pythonscheduler",

    # Version number (initial):
    version="0.1.0",

    # Application author details:
    author='Amit Kumar',
    author_email='amit2k5@gmail.com',

    # Packages
    packages=["app"],

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="https://gitlab.com/amit2k5/pythonscheduler.git",

    #
    # license="LICENSE.txt",
    description="Python Scheduler",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "datetime", "python-crontab", "pip",
    ],
)
